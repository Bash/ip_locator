## IP Locator
### USAGE:
You will need Python 3+ in order to work properly.

Install dependencies:
```
pip install -r requirements.txt
```
Open config.py and insert the required data:
- Your Telegram user id.
- Your bot's token provided by BotFather.
- An API key from any lookup service(you may need to change the code.)

I personally used https://ipgeolocation.io/ free API service limited by 1000 daily requests.